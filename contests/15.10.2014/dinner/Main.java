import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/*
    Test generator for Dinner problem.
    Sholokhov Artem <sholokhov.aa@gmail.com>
*/

public class Main {

    private boolean solve(Object[] costsObjects, int total){
        int costs[] = new int[costsObjects.length];
        for (int i = 0; i < costsObjects.length; i++) {
            costs[i] = (Integer)costsObjects[i];
        }

        int fullSum = 0;
        for (Integer c : costs) {
            fullSum += c;
        }

        for (Integer c : costs) {
            if (fullSum - c >= total) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        int testCount = 20;
        Random rand = new Random();
        int limit = 5;

        try {
            for (int i = 0; i < testCount; i++) {
                System.out.println("Test #" + (i + 1) + " with limit " + limit);

                PrintWriter in = new PrintWriter(new File((i + 1) + ".in"));
                PrintWriter out = new PrintWriter(new File((i + 1) + ".out"));

                int moneyCount = rand.nextInt(limit) + 1;
                System.out.println("Money count is " + moneyCount);

                List<Integer> moneyList = new LinkedList<Integer>();
                for (int j = 0; j < moneyCount; j++) {
                    moneyList.add(rand.nextInt(limit));
                }

                int totalCost = rand.nextInt(limit) + 1;
                System.out.println("Total cost is " + totalCost);

                in.println(totalCost + " " + moneyCount);
                for (Integer c : moneyList) in.print(c + " ");

                long start = System.nanoTime();
                boolean solution = new Main().solve(moneyList.toArray(), moneyCount);
                long finish = System.nanoTime();

                out.println(solution ? "Yes" : "No");

                System.out.println("Solution is " + solution + "; time = " + ((finish - start)/1000000000.0) + "sec");
                System.out.println("---");

                limit *= 2;

                in.close();
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
