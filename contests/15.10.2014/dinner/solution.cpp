#include <cstdlib>
#include <vector>
#include <fstream>
 
using namespace std;
 
int main(int argc, char** argv) {
    ifstream in("dinner.in");
    ofstream out("dinner.out");
     
    long total;
    int n;
    vector <int> costs;
     
    in >> total >> n;
    long full = 0;
    for (int i = 0; i < n; i++) {
        int t;
        in >> t;
        full += t;
        costs.push_back(t);
    }
     
    for (int i = 0; i < n; i++) {
        if (full - costs[i] >= total) {
            out << "Yes" << endl;
            return 0;
        }
    }
     
    out << "No" << endl;
     
    return 0;
}
